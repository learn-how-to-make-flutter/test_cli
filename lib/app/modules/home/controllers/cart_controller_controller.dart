import 'package:get/get.dart';

import '../../product.dart';

class CartControllerController extends GetxController {
  //TODO: Implement CartControllerController

  final cartItems = [].obs;
  int get count => cartItems.length;
  double get totalPrice => cartItems.fold(0, (sum, item) => sum + item.price);

  addToCart(Product product) {
    cartItems.add(product);
  }
}
