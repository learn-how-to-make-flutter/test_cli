import 'package:get/get.dart';

import 'package:flutter_application_text_cli/app/modules/home/controllers/cart_controller_controller.dart';

import '../controllers/home_controller.dart';

class HomeBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<CartControllerController>(
      () => CartControllerController(),
    );
    Get.lazyPut<HomeController>(
      () => HomeController(),
    );
  }
}
